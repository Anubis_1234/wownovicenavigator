package com.wow.novice.navigator.info.app

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun el_15(menu_: () -> Unit, back: () -> Unit) {

    val width = LocalConfiguration.current.screenWidthDp
    val height = LocalConfiguration.current.screenHeightDp

    var text = "If you follow all the recommendations above, you will have everything you need to start playing " +
            "World of Warcraft. Luckily, most of this isn't too difficult to accomplish, so you can dive right " +
            "into the gameplay without fear of making any fatal mistakes. Take your time and enjoy it.\n\nSome " +
            "old locations in the game have remained unremastered, but World of Warcraft remains the most ambitious MMO " +
            "among similar games in this genre. You won't fully explore his world in a week, so be prepared for a " +
            "journey that could last months, or even years.\n\nWe hope this guide was useful to you, look for " +
            "more guides for all games on our website! Earn fame and fortune with your blade, and enjoy your game in " +
            "World of Warcraft!"
    var text_img = R.drawable.text_el15
    var img = R.drawable.el_15

    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(id = R.drawable.backgrouind), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    ) {
                    Image(painter = painterResource(id = R.drawable.back), contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Top)
                            .size((width * 0.15).dp, (height * 0.08).dp)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { back() }
                        )
                    
                    Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
                        modifier = Modifier
                            .padding(start = (height * 0.062).dp)
                            .size((width * 0.5).dp, (height * 0.08).dp)

                        )
                }

                Image(painter = painterResource(id = text_img), contentDescription = "",
                    modifier = Modifier.size((width * 0.8).dp, (height * 0.037).dp)
                    )

                Image(painter = painterResource(id = img), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.9).dp, (height * 0.32).dp)
                    )

                Box(modifier = Modifier.size((width * 0.9).dp, (height * 0.31).dp)) {
                    Text(
                        text = text,
                        modifier = Modifier.verticalScroll(ScrollState(0)),
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                }

                Image(painter = painterResource(id = R.drawable.button_to_menu), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.5).dp, (height * 0.1).dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() }
                        ) { menu_() }
                    )
            }
        }
    }
}