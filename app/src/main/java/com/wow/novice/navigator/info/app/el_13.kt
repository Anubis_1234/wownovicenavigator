package com.wow.novice.navigator.info.app

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun el_13(next: () -> Unit, back: () -> Unit) {

    val width = LocalConfiguration.current.screenWidthDp
    val height = LocalConfiguration.current.screenHeightDp

    var text = "The interface in World of Warcraft is very flexible in customization, and this is " +
            "truly amazing. And using a variety of addons for the game, you can completely redesign the" +
            " interface to your taste or add some useful features to the game, for example, interactive guides" +
            " for bosses in dungeons. Installing addons can be confusing, as the variety is truly amazing!" +
            " While the standard WoW interface is fine to use, it's also a good idea to take the time to " +
            "install ElvUI to help you navigate difficult raids or dungeons. For example, in the standard " +
            "interface on the screen there are such necessary elements as a skill bar, your character’s health" +
            " bar, as well as the enemy’s health bar. When you are simultaneously trying to attack an enemy," +
            " evade his abilities and see his general condition, then all these actions can create certain " +
            "inconveniences for you in battle. The ElvUI addon will allow you to rearrange all these elements " +
            "in the most comfortable way for you, moving them from the edges of the screen closer to the center, " +
            "so that it will be convenient for you to see everything at once."
    var text_img = R.drawable.text_el13
    var img = R.drawable.el_13

    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(id = R.drawable.backgrouind), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    ) {
                    Image(painter = painterResource(id = R.drawable.back), contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Top)
                            .size((width * 0.15).dp, (height * 0.08).dp)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { back() }
                        )
                    
                    Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
                        modifier = Modifier
                            .padding(start = (height * 0.062).dp)
                            .size((width * 0.5).dp, (height * 0.08).dp)

                        )
                }

                Image(painter = painterResource(id = text_img), contentDescription = "",
                    modifier = Modifier.size((width * 0.8).dp, (height * 0.037).dp)
                    )

                Image(painter = painterResource(id = img), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.9).dp, (height * 0.32).dp)
                    )

                Box(modifier = Modifier.size((width * 0.9).dp, (height * 0.31).dp)) {
                    Text(
                        text = text,
                        modifier = Modifier.verticalScroll(ScrollState(0)),
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                }

                Image(painter = painterResource(id = R.drawable.button_next), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.5).dp, (height * 0.1).dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() }
                        ) { next() }
                    )
            }
        }
    }
}