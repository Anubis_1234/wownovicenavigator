package com.wow.novice.navigator.info.app

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun el_5(next: () -> Unit, back: () -> Unit) {

    val width = LocalConfiguration.current.screenWidthDp
    val height = LocalConfiguration.current.screenHeightDp

    var text = "Warriors are a melee class that have one tank specialization and the other two damage dealers. If you like to rush into the thick of battle at the forefront, then warriors are your choice;\u2028Paladins are guardians of light magic whose abilities are designed for close combat. Paladins can be tanks, healers, and damage dealers;\u2028Hunters can be both melee and ranged fighters, and they can also tame wild animals, turning them into their battle pets;\u2028Rogues have three damage-only specializations and have talents for stealth, subterfuge, and incredible burst damage;\u2028Priests have two different healing specializations, or can tap into the powers of darkness to deal impressive ranged damage;\u2028Shamans are masters of the elements, striking their enemies with lightning and fire. They have three specializations - melee fighter, ranged fighter and healer;\u2028Mages use ice, fire, and arcane magic to deal ranged damage;\u2028Warlocks have three specializations that focus only on dealing damage at a distance. They use dark and fire magic to cast their spells, and they can also summon various demons to fight for them;\u2028Monks have no equal in combat. They summon mist to help them deal melee damage, absorb it as a tank, or heal their party as a healer;\u2028Druids take on various animal forms and are the most versatile class, having all four specializations: melee, ranged, healer and tank;\u2028Demon Hunters are highly mobile melee specialists who use demonic energy to power their attacks. They have two specializations that allow them to be both tanks and fighters;\u2028Death Knights are the liberated servants of the Lich King who were previously under his influence. Using their runeblades, they can tank or fight in close combat."
    var text_img = R.drawable.text_el5
    var img = R.drawable.el_5

    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(id = R.drawable.backgrouind), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    ) {
                    Image(painter = painterResource(id = R.drawable.back), contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Top)
                            .size((width * 0.15).dp, (height * 0.08).dp)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { back() }
                        )
                    
                    Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
                        modifier = Modifier
                            .padding(start = (height * 0.062).dp)
                            .size((width * 0.5).dp, (height * 0.08).dp)

                        )
                }

                Image(painter = painterResource(id = text_img), contentDescription = "",
                    modifier = Modifier.size((width * 0.8).dp, (height * 0.037).dp)
                    )

                Image(painter = painterResource(id = img), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.9).dp, (height * 0.32).dp)
                    )

                Box(modifier = Modifier.size((width * 0.9).dp, (height * 0.31).dp)) {
                    Text(
                        text = text,
                        modifier = Modifier.verticalScroll(ScrollState(0)),
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                }

                Image(painter = painterResource(id = R.drawable.button_next), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.5).dp, (height * 0.1).dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() }
                        ) { next() }
                    )
            }
        }
    }
}