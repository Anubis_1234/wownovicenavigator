package com.wow.novice.navigator.info.app

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource

@Composable
fun loading_screen(open: () -> Unit) {
    val progress = remember {
        Animatable(0f)
    }

    LaunchedEffect(Unit)
    {
        progress.animateTo(1f, tween(1000), block = {
            if (progress.value == 1f){
                open.invoke()
            }

        })
    }

    Box(modifier = Modifier
        .fillMaxSize()
    ) {
        Image(painter = painterResource(id = R.drawable.start_img), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Image(painter = painterResource(id = R.drawable.loading), contentDescription = "")
        }

    }
}