package com.wow.novice.navigator.info.app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.wow.novice.navigator.info.app.ui.theme.WoWNoviceNavigatorTheme

var LOADING_SCREEN = "LOADING_SCREEN"
var START_SCREEN = "START_SCREEN"
var EL1 = "EL1"
var EL2 = "EL2"
var EL3 = "EL3"
var EL4 = "EL4"
var EL5 = "EL5"
var EL6 = "EL6"
var EL7 = "EL7"
var EL8 = "EL8"
var EL9 = "EL9"
var EL10 = "EL10"
var EL11 = "EL11"
var EL12 = "EL12"
var EL13 = "EL13"
var EL14 = "EL14"
var EL15 = "EL15"
var EXIT = "EXIT"

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val windocontrol = WindowCompat.getInsetsController(window, window.decorView)
            windocontrol.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            windocontrol.hide(WindowInsetsCompat.Type.statusBars())
            windocontrol.hide(WindowInsetsCompat.Type.navigationBars())
            windocontrol.hide(WindowInsetsCompat.Type.displayCutout())
            WoWNoviceNavigatorTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    var navcontroller = rememberNavController()

                    NavHost(navController = navcontroller, startDestination = LOADING_SCREEN) {
                        composable(LOADING_SCREEN) {
                            loading_screen(open = {navcontroller.navigate(START_SCREEN)})
                        }

                        composable(START_SCREEN) {
                            menu(start = {navcontroller.navigate(EL1)},
                                exit = {navcontroller.navigate(EXIT)}
                                )
                        }

                        composable(EL1) {
                            el_1(next = {navcontroller.navigate(EL2)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL2) {
                            el_2(next = {navcontroller.navigate(EL3)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL3) {
                            el_3(next = {navcontroller.navigate(EL4)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL4) {
                            el_4(next = {navcontroller.navigate(EL5)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL5) {
                            el_5(next = {navcontroller.navigate(EL6)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL6) {
                            el_6(next = {navcontroller.navigate(EL7)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL7) {
                            el_7(next = {navcontroller.navigate(EL8)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL8) {
                            el_8(next = {navcontroller.navigate(EL9)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL9) {
                            el_9(next = {navcontroller.navigate(EL10)},
                                back = {navcontroller.popBackStack()}
                            )
                        }


                        composable(EL10) {
                            el_10(next = {navcontroller.navigate(EL11)},
                                back = {navcontroller.popBackStack()}
                                )
                        }

                        composable(EL11) {
                            el_11(next = {navcontroller.navigate(EL12)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL12) {
                            el_12(next = {navcontroller.navigate(EL13)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL13) {
                            el_13(next = {navcontroller.navigate(EL14)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL14) {
                            el_14(next = {navcontroller.navigate(EL15)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EL15) {
                            el_15(menu_ = {navcontroller.navigate(START_SCREEN)},
                                back = {navcontroller.popBackStack()}
                            )
                        }

                        composable(EXIT) {
                            finishAffinity()
                        }
                    }
                }
            }
        }
    }
}

