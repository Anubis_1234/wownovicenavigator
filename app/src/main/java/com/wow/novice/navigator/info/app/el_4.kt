package com.wow.novice.navigator.info.app

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun el_4(next: () -> Unit, back: () -> Unit) {

    val width = LocalConfiguration.current.screenWidthDp
    val height = LocalConfiguration.current.screenHeightDp

    var text = "Each race in World of Warcraft has a number of special, unique “bonuses” that represent some unique abilities or add points to a certain craft. For example, humans can remove stun effects from themselves, and blood elves can silence other players. These bonuses are worth taking into account if you need to theoretically justify your choice, but you shouldn't worry too much about these differences between races. It's best if you choose a race that you like both in appearance and because of its history, this will be the best reason to continue playing as it. If you're playing with friends, make sure everyone chooses races that belong to the same faction, otherwise you won't be able to play together. Choosing a class often becomes a more daunting question. There are 12 classes in the game, each of which has from 2 to 4 specializations (in the game they are called “specs”, from the English specialization). They determine the set of abilities that the character will have and the role he will perform in the group. Do you want to go ahead of the entire group and absorb enemy damage like a tank? Or do you want to support the lives of players as a healer? Or maybe you prefer to deal incredible damage in melee or ranged combat (such specializations are called damage dealer)?"
    var text_img = R.drawable.text_el4
    var img = R.drawable.el_4

    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(id = R.drawable.backgrouind), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    ) {
                    Image(painter = painterResource(id = R.drawable.back), contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Top)
                            .size((width * 0.15).dp, (height * 0.08).dp)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { back() }
                        )
                    
                    Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
                        modifier = Modifier
                            .padding(start = (height * 0.062).dp)
                            .size((width * 0.5).dp, (height * 0.08).dp)

                        )
                }

                Image(painter = painterResource(id = text_img), contentDescription = "",
                    modifier = Modifier.size((width * 0.8).dp, (height * 0.037).dp)
                    )

                Image(painter = painterResource(id = img), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.9).dp, (height * 0.32).dp)
                    )

                Box(modifier = Modifier.size((width * 0.9).dp, (height * 0.31).dp)) {
                    Text(
                        text = text,
                        modifier = Modifier.verticalScroll(ScrollState(0)),
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                }

                Image(painter = painterResource(id = R.drawable.button_next), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.5).dp, (height * 0.1).dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() }
                        ) { next() }
                    )
            }
        }
    }
}