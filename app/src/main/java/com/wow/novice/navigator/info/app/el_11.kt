package com.wow.novice.navigator.info.app

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun el_11(next: () -> Unit, back: () -> Unit) {

    val width = LocalConfiguration.current.screenWidthDp
    val height = LocalConfiguration.current.screenHeightDp

    var text = "All MMOs are inherently social games, but over the years, World of Warcraft has become friendly to players who like to be alone. The “Group Finder” tool will automatically place you in a group to complete almost everything, except perhaps the most difficult game content; you can handle most of the tasks yourself. But everything becomes much more interesting if you do it with friends, and by playing alone and not participating in the gaming community, you are doing yourself a disservice. There are several ways to make friends while playing. The easiest way is to chat in the Search for Satellites chat (type /join Search for Satellites in the chat to join it). The “Trade” chat is more intended for the sale and exchange of various services and goods (to join it, write in the chat /join Trade). Saying “Hi” to a stranger isn't a bad idea, but don't be surprised if your desire to chat is ignored. The best way to make friends is to join various gaming communities, in and outside of the game. Some people like to hang out on forums dedicated to the game. There you can find a variety of game-related themed Discord servers that you can join."
    var text_img = R.drawable.text_el11
    var img = R.drawable.el_11

    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(id = R.drawable.backgrouind), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
            )

        Box(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    ) {
                    Image(painter = painterResource(id = R.drawable.back), contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.Top)
                            .size((width * 0.15).dp, (height * 0.08).dp)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { back() }
                        )
                    
                    Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
                        modifier = Modifier
                            .padding(start = (height * 0.062).dp)
                            .size((width * 0.5).dp, (height * 0.08).dp)

                        )
                }

                Image(painter = painterResource(id = text_img), contentDescription = "",
                    modifier = Modifier.size((width * 0.8).dp, (height * 0.037).dp)
                    )

                Image(painter = painterResource(id = img), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.9).dp, (height * 0.32).dp)
                    )

                Box(modifier = Modifier.size((width * 0.9).dp, (height * 0.31).dp)) {
                    Text(
                        text = text,
                        modifier = Modifier.verticalScroll(ScrollState(0)),
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                }

                Image(painter = painterResource(id = R.drawable.button_next), contentDescription = "",
                    modifier = Modifier
                        .size((width * 0.5).dp, (height * 0.1).dp)
                        .clickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() }
                        ) { next() }
                    )
            }
        }
    }
}